package com.example.dogwalking.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import com.example.dogwalking.R;
import com.example.dogwalking.activities.HomeMapActivity;
import com.example.dogwalking.helpers.UtilValidators;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.example.dogwalking.models.UserProfile;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RegisterFragment extends Fragment {
    public static final String TAG_REGISTER = "TAG_REGISTER";

    private FirebaseAuth auth;
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://dogwalking-f7488-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    private String roleStr = "USER";

    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;

    public static RegisterFragment newInstance() {
        Bundle args = new Bundle();

        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            ActivitiesFragmentsCommunication fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.user_profile_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateEmailAndPassword();
            }
        });


    }

    private void validateEmailAndPassword() {

        if (getView() == null) {
            return;
        }

        EditText emailEdtText = getView().findViewById(R.id.email_text_input_editText);
        EditText passwordEdtText = getView().findViewById(R.id.password_text_input_editText);

        String email = emailEdtText.getText().toString();
        String password = passwordEdtText.getText().toString();

        if (!UtilValidators.isValidEmail(email)) {
            emailEdtText.setError("Invalid Email");
            return;
        } else {
            emailEdtText.setError(null);
        }
        if (!UtilValidators.isValidPassword(password)) {
            passwordEdtText.setError("Invalid Password");
            return;
        } else {
            passwordEdtText.setError(null);
        }

        createDialog(email, password);


    }

    private void createFirebaseUser(String email, String password, String fullNameStr, String callingNameStr, String phoneStr, String addressStr, String roleStr) {

        if (getActivity() == null) {
            return;
        }

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = auth.getCurrentUser();
                            String userProfileId = user.getUid();
                            String userEmail = user.getEmail();
                            fetchCurrentLocation(userProfileId, userEmail, fullNameStr, callingNameStr, phoneStr, addressStr, roleStr, userProfileId);
                            Toast.makeText(getContext(), "Authentication success", Toast.LENGTH_SHORT).show();
                            gotoHomeMapActivity();
                        } else {
                            Toast.makeText(getContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


    private void createDialog(String email, String password) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog);

        EditText fullName = dialog.findViewById(R.id.nameTextInputEditText);
        EditText collingName = dialog.findViewById(R.id.callingNameTextInputEditText);
        EditText phoneNumber = dialog.findViewById(R.id.phone_text_input_editText);
        EditText address = dialog.findViewById(R.id.address_text_input_editText);
        RadioGroup rGroup = dialog.findViewById(R.id.rolesGroup);


        rGroup.setOnCheckedChangeListener((group, checkedId) -> {

            RadioButton checkedRadioButton = group.findViewById(checkedId);
            boolean isChecked = checkedRadioButton.isChecked();
            if (isChecked) {
                roleStr = checkedRadioButton.getText().toString();
            }
        });

        Button proceed =
                dialog.findViewById(R.id.button);

        proceed.setOnClickListener(view -> {

            if (TextUtils.isEmpty(fullName.getText().toString()) && TextUtils.isEmpty(collingName.getText().toString()) && TextUtils.isEmpty(phoneNumber.getText().toString()) && TextUtils.isEmpty(address.getText().toString())) {
                Toast.makeText(getContext(), "Invalid field", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    if (geoLocate(address.getText().toString()) != null) {
                        createFirebaseUser(email, password, fullName.getText().toString(), collingName.getText().toString(), phoneNumber.getText().toString(), address.getText().toString(), roleStr);
                        //Toast.makeText(getContext(), "Ok", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getContext(), "The address is not correct", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void createNewUser(String userProfileId, String email, String fullName, String collingName, String phoneNumber, String address, String role, String userId) {
        UserProfile userProfile = new UserProfile(email, fullName, collingName, phoneNumber, address, role, userId);
        myRef.child("users").child(userProfileId).setValue(userProfile);
    }

    public void gotoHomeMapActivity() {
        Intent intent = new Intent(getActivity(), HomeMapActivity.class);
        startActivity(intent);

        getActivity().finish();
    }

    public void fetchCurrentLocation(String userProfileId, String email, String fullName, String collingName, String phoneNumber, String address, String role, String userId) {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(location -> {

            if (location != null) {
                createNewUser(userProfileId, email, fullName, collingName, phoneNumber, address, role, userId);
            }
        });

    }

    public Address geoLocate(String addressString) throws IOException {

        Address address = null;
        Geocoder geocoder = new Geocoder(getContext());
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(addressString, 1);

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (list.size() > 0) {
            address = list.get(0);
        }
        return address;
    }

}


