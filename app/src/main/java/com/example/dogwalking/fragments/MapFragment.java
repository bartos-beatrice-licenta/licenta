package com.example.dogwalking.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.dogwalking.R;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.example.dogwalking.models.Point;
import com.example.dogwalking.models.Route;
import com.example.dogwalking.models.UserProfile;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;


public class MapFragment extends Fragment implements OnMapReadyCallback {
    public static final String TAG_MAP = "TAG_MAP";
    private ActivitiesFragmentsCommunication fragmentsCommunication;

    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://dogwalking-f7488-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    String currentUserRole;
    UserProfile currentUser;
    List<UserProfile> allUsers = new ArrayList<>();
    List<LatLng> locationArrayList = new ArrayList<>();

    Chronometer chronometer;
    Button startStopBtn;
    Boolean isStarted = false;
    String time;
    TextView distance;
    String currentDate;
    String currentTime;
    LatLng firstLocation;
    Double dist;

    private GoogleMap googleMap;
    private Marker currentMarker;
    private Polyline currentRoute;

    List<Point> currentLocations = new LinkedList<>();
    LocationManager locationManager;

    TextView userName;
    TextView userEmail;

    public static MapFragment newInstance() {
        Bundle args = new Bundle();

        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);
        View headerView=inflater.inflate(R.layout.drawer_header,container,false);
        userName=headerView.findViewById(R.id.user_name);
        userEmail=headerView.findViewById(R.id.user_email);
        userName.setText("name");
        userEmail.setText("email");
        startStopBtn = view.findViewById(R.id.start_stop_button);
        chronometer = view.findViewById(R.id.chronometer);
        distance=view.findViewById(R.id.distance_text);
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        startStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isStarted) {
                    googleMap.clear();
                    onMapReady(googleMap);
                    isStarted = true;
                    startStopBtn.setText("STOP");
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(Calendar.getInstance().getTime());
                    int hours = cal.get(Calendar.HOUR_OF_DAY);
                    int minutes = cal.get(Calendar.MINUTE);
                    currentTime=hours+":"+minutes;

                    startLocationsUpdate();
                    dist= (double) 0;
                    distance.setText(dist.toString());
                } else {
                    isStarted = false;
                    startStopBtn.setText("START");
                    chronometer.stop();

                    time=chronometer.getText().toString();
                    addRoute();
                    currentLocations.clear();
                    locationManager.removeUpdates(locationListenerGPS);

                }
            }
        });

        chronometer.setFormat("00:%s");
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
                if (elapsedMillis > 3600000L) {
                    chronometer.setFormat("0%s");
                } else {
                    chronometer.setFormat("00:%s");
                }
            }
        });
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        getUsers(new DataStatus() {
            @Override
            public void DataIsLoaded(UserProfile user) {

            }

            @Override
            public void UsersDataIsLoaded(ArrayList<UserProfile> users) {

                allUsers = users;
                for (int i = 0; i < allUsers.size(); i++) {
                    String addressStr = allUsers.get(i).getAddress().toString();
                    Address address = geoLocate(addressStr);
                    LatLng location = new LatLng(address.getLatitude(), address.getLongitude());
                    locationArrayList.add(location);

                }
            }
        });

        getCurrentUser(new DataStatus() {
            @Override
            public void DataIsLoaded(UserProfile user) {
                currentUser=user;
                currentUserRole = user.getRole();
                fetchLastLocation();

            }

            @Override
            public void UsersDataIsLoaded(ArrayList<UserProfile> users) {

            }
        });
    }

    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {
                    currentLocation = location;
                    firstLocation=new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
                    //Toast.makeText(getContext(), currentLocation.getLatitude() + " " + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    SupportMapFragment mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map);
                    mMapFragment.getMapAsync(MapFragment.this);
                }
            }
        });
    }
    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);

        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("I'm here!");
        PolylineOptions polylineOptions = new PolylineOptions();
        if (currentUserRole.equals("USER")) {
            markerOptions.icon(BitmapFromVector(getContext(), R.drawable.pets));
            currentMarker = googleMap.addMarker(markerOptions);
            currentRoute = googleMap.addPolyline(polylineOptions);
            for (int i = 0; i < locationArrayList.size(); i++) {

                if (allUsers.get(i).getRole().equals("DOG WALKER")) {
                    MarkerOptions markerOptionsDogUber = new MarkerOptions().position(locationArrayList.get(i)).title("Dog Walker");
                    markerOptionsDogUber.icon(BitmapFromVector(getContext(), R.drawable.emoji_people));
                    googleMap.addMarker(markerOptionsDogUber);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList.get(i)));
                }

            }

        } else if(currentUserRole.equals("DOG WALKER")){
            markerOptions.icon(BitmapFromVector(getContext(), R.drawable.pet_sitter));
            currentMarker = googleMap.addMarker(markerOptions);
            currentRoute = googleMap.addPolyline(polylineOptions);
            for (int i = 0; i < locationArrayList.size(); i++) {

                if (!allUsers.get(i).getRole().equals("USER")) {
                    MarkerOptions markerOptionsDogUber = new MarkerOptions().position(locationArrayList.get(i)).title("Dog Walker");
                    markerOptionsDogUber.icon(BitmapFromVector(getContext(), R.drawable.emoji_people));
                    googleMap.addMarker(markerOptionsDogUber);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(locationArrayList.get(i)));
                }

            }
        }

        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLastLocation();
                }
                break;
        }
    }

    public void getCurrentUser(final DataStatus dataStatus) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid = currentUser.getUid();
        DatabaseReference currentUserFromDatabase = myRef.child("users").child(currentUserUid);
        currentUserFromDatabase.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        UserProfile user = dataSnapshot.getValue(UserProfile.class);
                        dataStatus.DataIsLoaded(user);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // Log.d("TAG", "getUser:onCancelled", databaseError.toException());

                    }
                });

    }

    public void getUsers(final DataStatus dataStatus) {
        DatabaseReference currentUserFromDatabase = myRef.child("users");
        currentUserFromDatabase.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<UserProfile> users = new ArrayList<>();

                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            UserProfile data = ds.getValue(UserProfile.class);
                            UserProfile user = new UserProfile(data.getFullName(), data.getCallingName(), data.getPhoneNumber(), data.getAddress(), data.getRole(), data.getUserId());
                            users.add(user);

                        }
                        dataStatus.UsersDataIsLoaded(users);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

    }

    public Address geoLocate(String addressString) {

        Geocoder geocoder = new Geocoder(getContext());
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(addressString, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list.get(0);
    }

    public void startLocationsUpdate() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                5_000,
                1, locationListenerGPS);

    }

    android.location.LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            String msg = "New Latitude: " + latitude + "New Longitude: " + longitude;
            Log.d("Location", msg);
            currentLocations.add(new Point().setLatitude(latitude).setLongitude(longitude));
            addPointOnMap(location.getLatitude(), location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public double getDistanceCoordinates(double lat1, double long1, double lat2, double long2) {
        double earthRadius = 6371e3;
        double var1 = lat1 * Math.PI/180;
        double var2 = lat2 * Math.PI/180;
        double def1 = (lat2-lat1) * Math.PI/180;
        double def2 = (long2-long1) * Math.PI/180;
        double res1 = Math.sin(def1/2) * Math.sin(def1/2) +
                Math.cos(var1) * Math.cos(var2) *
                        Math.sin(def2/2) * Math.sin(def2/2);
        double res2 = 2 * Math.atan2(Math.sqrt(res1), Math.sqrt(1-res1));
        return earthRadius * res2;
    }


    private void addPointOnMap(double latitude, double longitude){
        LatLng currentPoint = new LatLng(latitude, longitude);
        currentMarker.setPosition(currentPoint);
        List<LatLng> points = currentRoute.getPoints();
        points.add(currentPoint);
        currentRoute.setPoints(points);

        float[] results = new float[1];
        Location.distanceBetween(firstLocation.latitude, firstLocation.longitude,
                currentPoint.latitude,currentPoint.longitude,
                results);
        double result = getDistanceCoordinates(firstLocation.latitude,firstLocation.longitude,
                currentPoint.latitude,currentPoint.longitude);
        if(result>10){
            result=0;
        }
        dist = dist+(double)result;
        Log.d("distance", String.valueOf(result));
        firstLocation=currentPoint;
        distance.setText(String.format("%.2f", dist));
    }

    private void addRoute() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid = currentUser.getUid();
        Route route = new Route(currentLocations,time,dist.toString(),currentDate,currentTime);
        myRef.child("routes").child(currentUserUid).child("routesLocations").push().setValue(route);
    }

    public interface DataStatus {
        void DataIsLoaded(UserProfile user);

        void UsersDataIsLoaded(ArrayList<UserProfile> users);
    }

}

