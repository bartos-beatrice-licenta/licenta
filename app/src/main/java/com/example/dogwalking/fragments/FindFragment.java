package com.example.dogwalking.fragments;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dogwalking.R;
import com.example.dogwalking.adapters.FindAdapter;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.example.dogwalking.models.UserProfile;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class FindFragment extends Fragment {
    public static final String TAG_FIND = "TAG_FIND";
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://dogwalking-f7488-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();

    List<UserProfile> users = new LinkedList<>();
    FindAdapter findAdapter = new FindAdapter(users);
    UserProfile currentUserFromDatabase;

    public static FindFragment newInstance() {
        Bundle args = new Bundle();

        FindFragment fragment = new FindFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getUsersFromDatabase();

        getCurrentUser(user -> currentUserFromDatabase=user);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication) {
            ActivitiesFragmentsCommunication fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_find, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView routesList = view.findViewById(R.id.recycler_view_find);
        routesList.setLayoutManager(linearLayoutManager);
        routesList.setAdapter(findAdapter);

        return view;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button allFilterButton = view.findViewById(R.id.all);
        Button nearFilterButton = view.findViewById(R.id.near);


        view.findViewById(R.id.all).setOnClickListener(view1 -> {
            allFilterButton.setTextColor(Color.rgb(249, 152, 39));
            allFilterButton.setBackground(getResources().getDrawable(R.drawable.filter_button_80_transparent));

            nearFilterButton.setTextColor(Color.rgb(255, 255, 255));
            nearFilterButton.setBackground(getResources().getDrawable(R.drawable.filter_button_transparent));

            getUsersFromDatabase();
        });

        view.findViewById(R.id.near).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nearFilterButton.setTextColor(Color.rgb(249, 152, 39));
                nearFilterButton.setBackground(getResources().getDrawable(R.drawable.filter_button_80_transparent));

                allFilterButton.setTextColor(Color.rgb(255, 255, 255));
                allFilterButton.setBackground(getResources().getDrawable(R.drawable.filter_button_transparent));
                getNearUsersFromDatabase();
            }
        });
    }

    public void getUsersFromDatabase() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid;
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
            DatabaseReference usersRef = myRef.child("users");

            users.clear();
            ValueEventListener eventListener = new ValueEventListener() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {

                        UserProfile user = ds.getValue(UserProfile.class);
                        if (user != null && !Objects.equals(user.getRole(), "USER") && !Objects.equals(user.getUserId(), currentUserUid)) {
                            users.add(user);
                            findAdapter.notifyDataSetChanged();
                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            };
            usersRef.addListenerForSingleValueEvent(eventListener);
        }
    }

    public void getCurrentUser(final FindFragment.GetData dataStatus) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid;
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
            DatabaseReference currentUserFromDatabase = myRef.child("users").child(currentUserUid);
            currentUserFromDatabase.addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            UserProfile user = dataSnapshot.getValue(UserProfile.class);
                            dataStatus.GetUser(user);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void getNearUsersFromDatabase() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid = currentUser.getUid();
        DatabaseReference usersRef = myRef.child("users");

        users.clear();
        ValueEventListener eventListener = new ValueEventListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    UserProfile user = ds.getValue(UserProfile.class);
                    if (user != null && !Objects.equals(user.getRole(), "USER") && !Objects.equals(user.getUserId(), currentUserUid)) {


                        Address userAddress = geoLocate(user.getAddress());
                        Address currentAddress = geoLocate(currentUserFromDatabase.getAddress());


                        double distance = getDistanceCoordinates(currentAddress.getLatitude(), currentAddress.getLongitude(), userAddress.getLatitude(), userAddress.getLongitude());
                        if (distance <= 1000) {
                            users.add(user);
                            findAdapter.notifyDataSetChanged();
                        }

                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        usersRef.addListenerForSingleValueEvent(eventListener);
        findAdapter.notifyDataSetChanged();

    }

    public Address geoLocate(String addressString) {

        Geocoder geocoder = new Geocoder(getContext());
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(addressString, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list.get(0);
    }

    public double getDistanceCoordinates(double lat1, double long1, double lat2, double long2) {
        double earthRadius = 6371e3;
        double var1 = lat1 * Math.PI / 180;
        double var2 = lat2 * Math.PI / 180;
        double def1 = (lat2 - lat1) * Math.PI / 180;
        double def2 = (long2 - long1) * Math.PI / 180;
        double res1 = Math.sin(def1 / 2) * Math.sin(def1 / 2) +
                Math.cos(var1) * Math.cos(var2) *
                        Math.sin(def2 / 2) * Math.sin(def2 / 2);
        double res2 = 2 * Math.atan2(Math.sqrt(res1), Math.sqrt(1 - res1));
        return earthRadius * res2;
    }

    public interface GetData {
        void GetUser(UserProfile user);

    }
}
