package com.example.dogwalking.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.dogwalking.R;
import com.example.dogwalking.helpers.UtilValidators;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.example.dogwalking.models.UserProfile;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EditProfile extends Fragment {
    public static final String TAG_EDIT = "TAG_EDIT";
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://dogwalking-f7488-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    UserProfile currentUser;

    EditText fullNameEditText;
    EditText callingNameEditText;
    EditText phoneEditText;
    EditText addressNameEditText;

    public static EditProfile newInstance() {
        Bundle args = new Bundle();

        EditProfile fragment = new EditProfile();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getCurrentUser(user -> {
            currentUser = user;
            fullNameEditText.setText(currentUser.getFullName());
            callingNameEditText.setText(currentUser.getCallingName());
            phoneEditText.setText(currentUser.getPhoneNumber());
            addressNameEditText.setText(currentUser.getAddress());
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            ActivitiesFragmentsCommunication fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fullNameEditText = view.findViewById(R.id.fullName_edit_editText);
        callingNameEditText = view.findViewById(R.id.callingName_edit_editText);
        phoneEditText = view.findViewById(R.id.phone_edit_editText);
        addressNameEditText = view.findViewById(R.id.address_edit_editText);

        view.findViewById(R.id.edit_profile_button).setOnClickListener(v -> validateFields());

    }

    public void getCurrentUser(final DataStatus dataStatus) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid;
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
            DatabaseReference currentUserFromDatabase = myRef.child("users").child(currentUserUid);
            currentUserFromDatabase.addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            UserProfile user = dataSnapshot.getValue(UserProfile.class);
                            dataStatus.DataIsLoaded(user);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }
    }

    private void updateUser() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid;
        if (currentUser != null) {
            currentUserUid = currentUser.getUid();
            myRef.child("users").child(currentUserUid)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            Map<String, Object> postValues = new HashMap<>();
                                                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                                                postValues.put(snapshot.getKey(), snapshot.getValue());
                                                            }

                                                            postValues.put("fullName", fullNameEditText.getText().toString());
                                                            postValues.put("callingName", callingNameEditText.getText().toString());
                                                            postValues.put("phoneNumber", phoneEditText.getText().toString());
                                                            postValues.put("address", addressNameEditText.getText().toString());

                                                            myRef.child("users").child(currentUserUid).updateChildren(postValues);

                                                            Toast.makeText(getContext(), "The information was successfully updated.", Toast.LENGTH_SHORT).show();

                                                            FragmentManager fragmentManager = getFragmentManager();
                                                            if (fragmentManager != null) {
                                                                fragmentManager.popBackStack();
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            Toast.makeText(getContext(), "Update Failed", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                    );
        }
    }

    private void validateFields() {
        if (getView() == null) {
            return;
        }
        String fullName = fullNameEditText.getText().toString();
        String callingName = callingNameEditText.getText().toString();
        String phone = phoneEditText.getText().toString();
        String address = addressNameEditText.getText().toString();

        if(Objects.equals(fullName, currentUser.getFullName()) && callingName.equals(currentUser.getCallingName()) && phone.equals(currentUser.getPhoneNumber()) && address.equals(currentUser.getAddress()))
        {
            Toast.makeText(getContext(), "Your data is not changed", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!UtilValidators.isNotEmptyInput(fullName)) {
            fullNameEditText.setError("Empty field");
            return;
        } else {
            fullNameEditText.setError(null);
        }
        if (!UtilValidators.isNotEmptyInput(callingName)) {
            callingNameEditText.setError("Empty field");
            return;
        } else {
            callingNameEditText.setError(null);
        }
        if (!UtilValidators.isNotEmptyInput(phone)) {
            phoneEditText.setError("Empty field");
            return;
        } else {
            phoneEditText.setError(null);
        }
        if (!UtilValidators.isNotEmptyInput(address)) {
            addressNameEditText.setError("Empty field");
            return;
        } else {
            addressNameEditText.setError(null);
        }

        updateUser();
    }

    public interface DataStatus {
        void DataIsLoaded(UserProfile user);
    }

}
