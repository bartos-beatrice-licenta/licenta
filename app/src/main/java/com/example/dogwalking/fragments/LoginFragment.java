package com.example.dogwalking.fragments;
import static android.content.Context.MODE_PRIVATE;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.dogwalking.R;
import com.example.dogwalking.activities.HomeMapActivity;
import com.example.dogwalking.helpers.UtilValidators;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class LoginFragment extends Fragment {
    public static final String TAG_LOGIN = "TAG_LOGIN";

    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private FirebaseAuth auth;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EditText editTextEmail = view.findViewById(R.id.email_text_input_editText);
        EditText editTextPassword = view.findViewById(R.id.password_text_input_editText);
        CheckBox rememberMe = view.findViewById(R.id.remember_me_checkBox);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("mainPrefs", MODE_PRIVATE);


        if (sharedPreferences.contains("Email") && sharedPreferences.contains("Password")) {
            rememberMe.setChecked(true);
            String emailStr = sharedPreferences.getString("Email", "");
            String passwordStr = sharedPreferences.getString("Password", "");
            editTextEmail.setText(emailStr);
            editTextPassword.setText(passwordStr);
        }

        view.findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateEmailAndPassword();
            }
        });

        view.findViewById(R.id.forgotPassword_textView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goToForgotPasswordFragment();
            }
        });

        view.findViewById(R.id.register_textView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goToRegisterFragment();
            }
        });


        rememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {

                    save("Email", editTextEmail);
                    save("Password", editTextPassword);
                } else if (!compoundButton.isChecked()) {

                    delete();
                    delete();
                }
            }
        });

    }

    private void goToRegisterFragment() {

       // Toast.makeText(getContext(), "Go to Register Fragment", Toast.LENGTH_SHORT).show();
        if (fragmentsCommunication != null) {
            fragmentsCommunication.onReplaceFragment(RegisterFragment.TAG_REGISTER);
        }
    }

    private void goToForgotPasswordFragment() {

        //Toast.makeText(getContext(), "Go to Forgot Password Fragment", Toast.LENGTH_SHORT).show();
        if (fragmentsCommunication != null) {
            fragmentsCommunication.onReplaceFragment(ForgotPassword.TAG_FORGOT_PASS);
        }
    }

    private void goToHomeMapActivity() {
        Intent intent = new Intent(getActivity(), HomeMapActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void validateEmailAndPassword() {

        if (getView() == null) {
            return;
        }
        EditText emailEdtText = getView().findViewById(R.id.email_text_input_editText);
        EditText passwordEdtText = getView().findViewById(R.id.password_text_input_editText);

        String email = emailEdtText.getText().toString().trim();

        String password = passwordEdtText.getText().toString();

        if (!UtilValidators.isValidEmail(email)) {
            emailEdtText.setError("Invalid Email");
            return;
        } else {
            emailEdtText.setError(null);
        }
        if (!UtilValidators.isValidPassword(password)) {
            passwordEdtText.setError("Invalid Password");
            return;
        } else {
            passwordEdtText.setError(null);
        }

        loginFirebaseUser(email, password);
    }

    private void loginFirebaseUser(String email, String password) {

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    goToHomeMapActivity();
                    Toast.makeText(getContext(), "Authentication success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void save(String key, EditText text) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("mainPrefs", MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        myEdit.putString(key, text.getText().toString());
        myEdit.apply();
    }

    private void delete() {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("mainPrefs", MODE_PRIVATE);
        boolean myEdit = sharedPreferences.edit().clear().commit();
    }

}
