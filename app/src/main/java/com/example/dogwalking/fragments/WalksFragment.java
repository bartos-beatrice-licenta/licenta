package com.example.dogwalking.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dogwalking.R;
import com.example.dogwalking.adapters.LocationAdapter;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.example.dogwalking.models.Route;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.LinkedList;
import java.util.List;

public class WalksFragment extends Fragment {
    public static final String TAG_WALKS = "TAG_WALKS";

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://dogwalking-f7488-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();

    List<Route> routes = new LinkedList<>();
    LocationAdapter locationAdapter = new LocationAdapter(routes);

    public static WalksFragment newInstance() {
        Bundle args = new Bundle();

        WalksFragment fragment;
        fragment = new WalksFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataFromDatabase();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_walks, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView routesList = view.findViewById(R.id.recycler_view_map);
        routesList.setLayoutManager(linearLayoutManager);
        routesList.setAdapter(locationAdapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            ActivitiesFragmentsCommunication fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }


    public void getDataFromDatabase() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid = currentUser.getUid();
        DatabaseReference currentUserLocationsRef = myRef.child("routes").child(currentUserUid).child("routesLocations");

        routes.clear();
        ValueEventListener eventListener = new ValueEventListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Route route = ds.getValue(Route.class);
                    if(route.getLocations()!=null){
                        routes.add(route);
                        locationAdapter.notifyDataSetChanged();
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        currentUserLocationsRef.addListenerForSingleValueEvent(eventListener);
    }

}
