package com.example.dogwalking.fragments;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.dogwalking.R;
import com.example.dogwalking.helpers.UtilValidators;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPassword extends Fragment {
    public static final String TAG_FORGOT_PASS = "TAG_FORGOT_PASS";
    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private FirebaseAuth auth;

    public static ForgotPassword newInstance() {
        Bundle args = new Bundle();

        ForgotPassword fragment = new ForgotPassword();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recovery_pass, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.resetPassBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateEmail();
            }
        });
    }

    private void goToLoginFragment() {
        if (fragmentsCommunication != null) {
            fragmentsCommunication.onReplaceFragment(LoginFragment.TAG_LOGIN);
        }
    }

    private void validateEmail() {
        if (getView() == null) {
            return;
        }

        EditText emailEdtText = getView().findViewById(R.id.email_text_input_editText);
        String email = emailEdtText.getText().toString().trim();

        if (!UtilValidators.isValidEmail(email)) {
            emailEdtText.setError("Invalid Email");
            return;
        } else {
            emailEdtText.setError(null);
        }
        resetPassword(email);
    }

    private void resetPassword(String email) {
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "The email was sent successfully!", Toast.LENGTH_SHORT).show();
                            goToLoginFragment();
                        } else {
                            Toast.makeText(getContext(), "The email was not sent successfully!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}

