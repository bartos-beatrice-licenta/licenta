package com.example.dogwalking.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.dogwalking.R;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;

public class WelcomeFragment extends Fragment {
    public static final String TAG_WELCOME = "TAG_WELCOME";

    private ActivitiesFragmentsCommunication fragmentsCommunication;

    public static WelcomeFragment newInstance() {
        Bundle args = new Bundle();

        WelcomeFragment fragment;
        fragment = new WelcomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLoginFragment();
            }
        });

        view.findViewById(R.id.register_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegisterFragment();
            }
        });

    }

    private void goToLoginFragment() {

        if (fragmentsCommunication != null) {
            fragmentsCommunication.onReplaceFragment(LoginFragment.TAG_LOGIN);
        }
    }

    private void goToRegisterFragment() {

        if (fragmentsCommunication != null) {
            fragmentsCommunication.onReplaceFragment(RegisterFragment.TAG_REGISTER);
        }
    }
}

