package com.example.dogwalking.helpers;

import android.text.TextUtils;
import android.util.Patterns;

public class UtilValidators {
    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.length() >= 6;
    }

    public static boolean isNotEmptyInput(String input) {
        return !TextUtils.isEmpty(input);
    }
}
