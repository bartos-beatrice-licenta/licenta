package com.example.dogwalking.models;


import java.util.List;

public class Route {

    private List<Point> locations;
    private String time;
    private String distance;
    private String currentDate;
    private String currentTime;

    public Route() {
    }

    public Route(List<Point> locations, String time, String distance, String currentDate, String currentTime) {
        this.locations = locations;
        this.time = time;
        this.distance = distance;
        this.currentDate = currentDate;
        this.currentTime = currentTime;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public String getDistance() {
        return distance;
    }

    public List<Point> getLocations() {
        return locations;
    }

    public String getTime() {
        return time;
    }
}
