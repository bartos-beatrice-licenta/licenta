package com.example.dogwalking.models;

public class Point {

    private Double latitude;
    private Double longitude;

    public Point(){
    }

    public Double getLatitude() {
        return latitude;
    }

    public Point setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Point setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }
}
