package com.example.dogwalking.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class UserProfile {
    String email;
    String fullName;
    String callingName;
    String phoneNumber;
    String address;
    String role;
    String userId;

    public String getEmail() {
        return email;
    }

    public UserProfile setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserProfile(String email, String fullName, String callingName, String phoneNumber, String address, String role, String userId) {
        this.email = email;
        this.fullName = fullName;
        this.callingName = callingName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.role = role;
        this.userId = userId;
    }

    public UserProfile(String fullName, String callingName, String phoneNumber, String address, String role, String userId) {
        this.fullName = fullName;
        this.callingName = callingName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.role = role;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserProfile() {
    }

    public String getCallingName() {
        return callingName;
    }

    public void setCallingName(String callingName) {
        this.callingName = callingName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
