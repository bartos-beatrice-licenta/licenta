package com.example.dogwalking.activities;

import static com.example.dogwalking.fragments.ForgotPassword.TAG_FORGOT_PASS;
import static com.example.dogwalking.fragments.LoginFragment.TAG_LOGIN;
import static com.example.dogwalking.fragments.RegisterFragment.TAG_REGISTER;
import static com.example.dogwalking.fragments.WelcomeFragment.TAG_WELCOME;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.example.dogwalking.R;
import com.example.dogwalking.fragments.ForgotPassword;
import com.example.dogwalking.fragments.LoginFragment;
import com.example.dogwalking.fragments.RegisterFragment;
import com.example.dogwalking.fragments.WelcomeFragment;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;

public class AuthActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (savedInstanceState == null) {
            onAddWelcomeFragment();
        }
    }

    private void onAddWelcomeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.auth_frame_layout, WelcomeFragment.newInstance(), TAG_WELCOME);

        fragmentTransaction.commit();
    }

    @Override
    public void onReplaceFragment(String TAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (TAG) {
            case TAG_LOGIN: {
                fragment = LoginFragment.newInstance();
                break;
            }
            case TAG_REGISTER: {
                fragment = RegisterFragment.newInstance();
                break;
            }
            case TAG_FORGOT_PASS: {
                fragment = ForgotPassword.newInstance();
                break;
            }
            case TAG_WELCOME: {
                fragment = WelcomeFragment.newInstance();
                break;
            }

            default:
                return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.auth_frame_layout, fragment, TAG);

        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }
}