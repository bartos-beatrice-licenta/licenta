package com.example.dogwalking.activities;
import static com.example.dogwalking.fragments.EditProfile.TAG_EDIT;
import static com.example.dogwalking.fragments.FindFragment.TAG_FIND;
import static com.example.dogwalking.fragments.MapFragment.TAG_MAP;
import static com.example.dogwalking.fragments.WalksFragment.TAG_WALKS;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.example.dogwalking.R;
import com.example.dogwalking.fragments.EditProfile;
import com.example.dogwalking.fragments.FindFragment;
import com.example.dogwalking.fragments.MapFragment;
import com.example.dogwalking.fragments.WalksFragment;
import com.example.dogwalking.interfaces.ActivitiesFragmentsCommunication;
import com.example.dogwalking.models.UserProfile;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class HomeMapActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://dogwalking-f7488-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference();
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    NavigationView navigationView;
    UserProfile currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_map);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        NavigationView navigationView = findViewById(R.id.navigationView);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.user_name);
        TextView navUserEmail = headerView.findViewById(R.id.user_email);

        getCurrentUser(new MapFragment.DataStatus() {
            @Override
            public void DataIsLoaded(UserProfile user) {
                currentUser=user;
                navUsername.setText(currentUser.getFullName());
                if(currentUser.getEmail()!=null){
                    navUserEmail.setText(currentUser.getEmail());
                }else{
                    navUserEmail.setText("email:null");
                }

            }

            @Override
            public void UsersDataIsLoaded(ArrayList<UserProfile> users) {

            }
        });

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dashboard");
        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.navigationView);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.walks) {
                    onReplaceFragment(TAG_WALKS);
                }
                if (item.getItemId() == R.id.newWalk) {
                    onReplaceFragment(TAG_MAP);
                }
                if (item.getItemId() == R.id.find_dog_uber) {
                    onReplaceFragment(TAG_FIND);
                }
                if (item.getItemId() == R.id.edit_profile) {
                    onReplaceFragment(TAG_EDIT);
                }
                if (item.getItemId() == R.id.logout) {
                    FirebaseAuth.getInstance().signOut();
                    goToWelcome();
                }
                return false;

            }
        });

        if (savedInstanceState == null) {
            onAddMapFragment();

        }

    }

    private void onAddMapFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;
        fragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_fragment, fragment, TAG_MAP);
        fragmentTransaction.commit();
    }

    private void goToWelcome() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onReplaceFragment(String TAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (TAG) {
            case TAG_WALKS: {
                fragment = WalksFragment.newInstance();
                break;
            }
            case TAG_MAP: {
                fragment = MapFragment.newInstance();
                break;
            }
            case TAG_EDIT: {
                fragment = EditProfile.newInstance();
                break;
            }
            case TAG_FIND: {
                fragment = FindFragment.newInstance();
                break;
            }

            default:
                return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_fragment, fragment, TAG);
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }

    public void getCurrentUser(final MapFragment.DataStatus dataStatus) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUid = currentUser.getUid();
        DatabaseReference currentUserFromDatabase = myRef.child("users").child(currentUserUid);
        currentUserFromDatabase.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        UserProfile user = dataSnapshot.getValue(UserProfile.class);
                        dataStatus.DataIsLoaded(user);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });

    }
}
