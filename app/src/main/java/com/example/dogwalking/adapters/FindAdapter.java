package com.example.dogwalking.adapters;

import android.content.ClipboardManager;
import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.dogwalking.R;
import com.example.dogwalking.models.UserProfile;

import java.util.List;

public class FindAdapter extends RecyclerView.Adapter<FindAdapter.FindViewHolder> {
    private List<UserProfile> users;

    public FindAdapter(List<UserProfile> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public FindViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());


        View view =inflater.inflate(R.layout.find_cell_item,parent,false);
        FindViewHolder findViewHolder =new FindViewHolder(view);

        return findViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FindViewHolder holder, int position) {

        UserProfile user= users.get(position);
        holder.bind(user);
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

    class FindViewHolder extends RecyclerView.ViewHolder{

        private TextView fullName;
        private TextView email;
        private TextView phone;
        private TextView address;
        private TextView addressIcon;
        private TextView fullNameIcon;
        private TextView emailIcon;
        private TextView phoneIcon;
        private View view;

        public FindViewHolder(View view)
        {
            super(view);
            fullName =view.findViewById(R.id.fullName);
            email=view.findViewById(R.id.email);
            phone=view.findViewById(R.id.phone);
            address=view.findViewById(R.id.address);
            addressIcon=view.findViewById(R.id.address_icon);
            phoneIcon=view.findViewById(R.id.phone_icon);
            emailIcon=view.findViewById(R.id.email_icon);
            fullNameIcon=view.findViewById(R.id.fullName_icon);
            this.view=view;
        }

        void bind(UserProfile userObj)
        {

            fullName.setText(userObj.getFullName());
            email.setText(userObj.getEmail());
            phone.setText(userObj.getPhoneNumber());
            address.setText(userObj.getAddress());

            addressIcon.setOnClickListener(v -> {
                Context context= itemView.getContext();
                ClipboardManager clipboardManager=(ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData=ClipData.newPlainText("TextView",address.getText().toString());
                clipboardManager.setPrimaryClip(clipData);
                Toast.makeText(context, "Address Copied.", Toast.LENGTH_SHORT).show();
            });

            phoneIcon.setOnClickListener(v -> {
                Context context= itemView.getContext();
                ClipboardManager clipboardManager=(ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData=ClipData.newPlainText("TextView",phone.getText().toString());
                clipboardManager.setPrimaryClip(clipData);
                Toast.makeText(context, "Phone Number Copied.", Toast.LENGTH_SHORT).show();
            });

            fullNameIcon.setOnClickListener(v -> {
                Context context= itemView.getContext();
                ClipboardManager clipboardManager=(ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData=ClipData.newPlainText("TextView",fullName.getText().toString());
                clipboardManager.setPrimaryClip(clipData);
                Toast.makeText(context, "Full Name Copied.", Toast.LENGTH_SHORT).show();
            });

            emailIcon.setOnClickListener(v -> {
                Context context= itemView.getContext();
                ClipboardManager clipboardManager=(ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData=ClipData.newPlainText("TextView",email.getText().toString());
                clipboardManager.setPrimaryClip(clipData);
                Toast.makeText(context, "Email Address Copied.", Toast.LENGTH_SHORT).show();
            });
        }
    }
}