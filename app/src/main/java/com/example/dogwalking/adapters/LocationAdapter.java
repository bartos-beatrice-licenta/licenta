package com.example.dogwalking.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dogwalking.R;
import com.example.dogwalking.models.Route;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.LinkedList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> {

    private List<Route> routes;
    public LocationAdapter(List<Route> routes) {
        this.routes = routes;
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.map_cell_item, parent, false);
        LocationViewHolder locationViewHolder = new LocationViewHolder(view);

        return locationViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {

        Route route = routes.get(position);
        holder.bind(route);
    }

    @Override
    public int getItemCount() {
        return this.routes.size();
    }

    static class LocationViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

        private TextView distance;
        private TextView time;
        private TextView currentTime;
        private TextView currentDate;
        private MapView mapView;
        private GoogleMap map;
        private View view;

       private Route route = new Route();
       private PolylineOptions polyline;
       private MarkerOptions markerOptionsDogUber;

        public LocationViewHolder(View view) {
            super(view);
            distance = view.findViewById(R.id.distance);
            time = view.findViewById(R.id.time);
            currentTime = view.findViewById(R.id.currentTime);
            currentDate = view.findViewById(R.id.date);
            mapView = view.findViewById(R.id.mapView);

            if (mapView != null) {
                mapView.onCreate(null);
                mapView.getMapAsync(this);
            }

            this.view = view;
        }

        void bind(Route routeObj) {
            if (routeObj.getDistance() != null) {
                double dist=Double.parseDouble(routeObj.getDistance());
                distance.setText(String.format("%.2f", dist));
            } else {
                distance.setText("0,0");
            }

            view.setTag(this);
            mapView.setTag(routeObj);

            route = routeObj;
            time.setText(route.getTime());
            currentDate.setText(route.getCurrentDate());
            currentTime.setText(route.getCurrentTime());
            onMapReady(map);
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(view.getContext());
            this.map = googleMap;

            setMapLocation(route);
        }

        private void setMapLocation(Route route) {
            if (route == null) return;
            if (map == null) return;

            if (route != null) {


                List<LatLng> list = new LinkedList<>();
                polyline = new PolylineOptions().width(5).color(Color.BLACK).geodesic(true);
                for (int i = 0; i < route.getLocations().size(); i++) {
                    list.add(new LatLng(route.getLocations().get(i).getLatitude(), route.getLocations().get(i).getLongitude()));
                    Log.d("cords", new LatLng(route.getLocations().get(i).getLatitude(), route.getLocations().get(i).getLongitude()).toString());

                }

                polyline.addAll(list);
                map.addPolyline(polyline);
                markerOptionsDogUber = new MarkerOptions().position(list.get(0)).title("I'm here");
                markerOptionsDogUber.icon(BitmapFromVector(view.getContext(), R.drawable.pet_sitter));
                map.addMarker(markerOptionsDogUber);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.getLocations().get(0).getLatitude(), route.getLocations().get(0).getLongitude()), 15.5f));
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }

        }

        private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {

            Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
            vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
            Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            vectorDrawable.draw(canvas);

            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }
    }
}